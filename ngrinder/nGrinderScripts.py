# -*- coding:utf-8 -*-
from net.grinder.script.Grinder import grinder
from net.grinder.script import Test
from net.grinder.plugin.http import HTTPRequest
from net.grinder.plugin.http import HTTPPluginControl
from HTTPClient import NVPair, Cookie, CookieModule, CookiePolicyHandler
from org.json import JSONObject, JSONArray
from time import time
from org.slf4j import LoggerFactory
from ch.qos.logback.classic import Level
from ch.qos.logback.classic import Logger
from java.lang import Exception
import random
import csv
import base64
import sys
from datetime import datetime
import uuid

sys.setdefaultencoding('utf-8')

log = grinder.logger.info
err = grinder.logger.error

# 액션 호출시 think time (sleep) 을 사용할지 설정
useThinkTime = True

# Timeout
control = HTTPPluginControl.getConnectionDefaults()
control.setTimeout(60000)

# 웹서버 Load Balancer가 없을 경우 배열에서 하나의 서버를 선택해서 액션을 호출하도록 함.
requestList = [
    HTTPRequest(url = "http://127.0.0.1")
]

test_login = Test(1, "login")
test_entrance = Test(2, "entrance")
test_dailyspinretry = Test(3, "dailyspinretry")
test_leaderboards = Test(4, "leaderboards")
test_profile = Test(5, "profile")
test_setprofile = Test(6, "setprofile")
test_getmessage = Test(7, "getmessage")
test_getfriends = Test(8, "getfriends")
test_getinvitelist = Test(9, "getinvitelist")
test_join = Test(10, "join")
test_addreel = Test(11, "addreel")
test_tableinfo = Test(12, "tableinfo")
test_leave = Test(13, "leave")
test_recentdata = Test(14, "recentdata")
test_addfriend = Test(15, "addfriend")
test_removefriend = Test(16, "removefriend")
test_buygiftshop = Test(17, "buygiftshop")
test_getstore = Test(18, "getstore")
test_googlepurchase = Test(19, "googlepurchase")

class TestRunner:
    def __init__(self):
        grinder.statistics.delayReports = True

        logger = LoggerFactory.getLogger("worker")
        logger.setLevel(Level.ERROR)
        totalProcessCount = grinder.getProperties().getInt("grinder.processes", 1)
        totalThreadCount = grinder.getProperties().getInt("grinder.threads", 1)
        agentNumber = grinder.agentNumber
        processNumber = grinder.processNumber
        threadNumber = grinder.threadNumber
        self.curNumber = (agentNumber * totalProcessCount * totalThreadCount) + (processNumber * totalThreadCount) + threadNumber + 1

        # 플랫폼 ID
        self.pid = str(self.curNumber + 100000000000000001)

        # 캐릭터 ID
        self.characterID = ''

        # request ID
        self.reqID = 1

        # 엑세스 토큰
        self.accessToken = str(uuid.uuid1()).replace("-", "")

        # 친구 추가, 삭제시 사용되는 친구 플랫폼 ID
        self.fpid = random.randrange(110000000000000001,110000000000001000)

        pass

    def initialSleep(self):
        sleeptime = grinder.threadNumber * 1000
        self.sleep(sleeptime)

    def parseJson(self, jsonString):
        return JSONObject(jsonString.getText().encode('utf-8'))

    def applyStatistics(self, result):
        if result.getStatusCode() == 200:
            jsonObject = self.parseJson(result)
            strResult = jsonObject.getString('success')
            if strResult == "true":
                grinder.statistics.forLastTest.success = 1
            else:
                err("result is not True : %s " % jsonObject)
                grinder.statistics.forLastTest.success = 0
        else:
            grinder.statistics.forLastTest.success = 0

    def sleep(self, millisecond):
        if useThinkTime:
            grinder.sleep(millisecond)

    # Test method
    def __call__(self):
        if grinder.runNumber == 0:
            self.initialSleep()

        tid = grinder.threadNumber % 10
        log("tid : %s" % tid)
        log("pid : %s " % self.pid)

        # 로그인
        request = self.getRequest()
        test_login.record(request)
        result = self.login(request)
        self.applyStatistics(result)
        jsonObject = self.parseJson(result)
        self.characterID = jsonObject.get('result').get('characterInfo').getString('characterID')
        characterChips = int(jsonObject.get('result').get('characterInfo').getString('characterChips'))
        characterTicket = int(jsonObject.get('result').get('characterInfo').getString('characterTicket'))
        log("characterID : %s " % self.characterID)
        self.sleep(5000)

        # 유저의 칩과 티켓 확인 후 올려주기
        if characterChips < 10000 or characterTicket < 10000:
            request = self.getRequest()
            self.setUserChipAndTicket(request)

        # 로비 진입
        request = self.getRequest()
        test_entrance.record(request)
        result = self.entrance(request)
        self.applyStatistics(result)
        log("entrance")
        self.sleep(3000)
        
        # 데일리스핀
        request = self.getRequest()
        test_dailyspinretry.record(request)
        result = self.dailyspinretry(request)
        self.applyStatistics(result)
        log("dailyspinretry")
        self.sleep(5000)

        # 리더보드 조회(랭킹)
        request = self.getRequest()
        test_leaderboards.record(request)
        result = self.leaderboards(request)
        self.applyStatistics(result)
        log("leaderboards")
        self.sleep(3000)

        # 프로필 조회
        request = self.getRequest()
        test_profile.record(request)
        result = self.profile(request)
        self.applyStatistics(result)
        log("profile")
        self.sleep(3000)

        # 프로필 수정
        request = self.getRequest()
        test_setprofile.record(request)
        result = self.setprofile(request)
        self.applyStatistics(result)
        log("setprofile")
        self.sleep(5000)

        # 우편함 조회
        request = self.getRequest()
        test_getmessage.record(request)
        result = self.getmessage(request)
        self.applyStatistics(result)
        log("getmessage")
        self.sleep(3000)

        # 친구목록 조회
        request = self.getRequest()
        test_getfriends.record(request)
        result = self.getfriends(request)
        self.applyStatistics(result)
        log("getfriends")
        self.sleep(3000)

        # 친구 요청 조회
        request = self.getRequest()
        test_getinvitelist.record(request)
        result = self.getinvitelist(request)
        self.applyStatistics(result)
        log("getinvitelist")
        self.sleep(3000)

        # 친구 추가
        request = self.getRequest()
        test_addfriend.record(request)
        result = self.addfriend(request)
        self.applyStatistics(result)
        log("addfriend")
        self.sleep(3000)

        # 친구 삭제
        request = self.getRequest()
        test_removefriend.record(request)
        result = self.removefriend(request)
        self.applyStatistics(result)
        log("removefriend")
        self.sleep(3000)

        # 슬롯 조인
        slotId = 9
        request = self.getRequest()
        test_join.record(request)
        joinResult = self.join(request, slotId)
        self.applyStatistics(joinResult)
        joinResultObject = self.parseJson(joinResult)
        tableId = joinResultObject.get('result').get('tableInfo').getString('tableId')
        log("join tableId : %s" % tableId)
        self.sleep(3000)

        # 룸에서 기프트 아이템 구매
        request = self.getRequest()
        test_buygiftshop.record(request)
        result = self.buygiftshop(request, tableId)
        self.applyStatistics(result)
        log("buygiftshop tableId : %s" % tableId)
        self.sleep(13000)

        for i in range(1, 10):
            # 슬롯 릴 (n회)
            request = self.getRequest()
            test_addreel.record(request)
            result = self.addreel(request, slotId, tableId)
            self.applyStatistics(result)
            log("addreel tableId : %s" % tableId)
            self.sleep(5000)

            # 슬롯 정보 조회 (n회)
            request = self.getRequest()
            test_tableinfo.record(request)
            result = self.tableinfo(request, slotId, tableId)
            self.applyStatistics(result)
            log("tableinfo tableId : %s" % tableId)
            self.sleep(2000)

        # 슬롯 나가기
        request = self.getRequest()
        test_leave.record(request)
        result = self.leave(request, slotId, tableId)
        self.applyStatistics(result)
        log("leave tableId : %s" % tableId)
        self.sleep(3000)

        # 로비 진입
        request = self.getRequest()
        test_entrance.record(request)
        result = self.entrance(request)
        self.applyStatistics(result)
        log("entrance")
        self.sleep(3000)

        # 로비 데이터 (룸 초대 목록)
        request = self.getRequest()
        test_recentdata.record(request)
        result = self.recentdata(request)
        self.applyStatistics(result)
        log("recentdata")
        self.sleep(2000)

        # 인앱결제 상품 조회
        request = self.getRequest()
        test_getstore.record(request)
        result = self.getstore(request)
        self.applyStatistics(result)
        log("getstore")
        self.sleep(3000)

        # 구글 인앱결제 (가중치)
        if tid in range(1, 2):
            # 결제준비(트랜잭션ID발급)
            request = self.getRequest()
            result = self.googlepurchaseprepare(request)
            googlePurchaseprepareResultObject = self.parseJson(result)
            log("googlepurchaseprepare")
            transactionId = googlePurchaseprepareResultObject.get('result').get('transactionInfo').getString('transactionId')
            cTime = str(int(time()))
            orderId = "GPA."+cTime
            purchaseTime = cTime

            # 결제정보 기록
            request = self.getRequest()
            result = self.googlepurchase(request, orderId, transactionId, purchaseTime, "N")
            self.applyStatistics(result)
            log("googlepurchase")

            # 결제 완료
            request = self.getRequest()
            test_googlepurchase.record(request)
            result = self.googlepurchase(request, orderId, transactionId, purchaseTime, "Y")
            self.applyStatistics(result)
            log("googlepurchase")
            

    # reqID 만들기
    def incrReqID(self):
        self.reqID = self.reqID + 1
        return str(self.reqID)

    # HTTP 서버 선택
    def getRequest(self):
        return random.choice(requestList)

    # API URL 만들기
    def getUri(self, api):
        return api+"/"+self.characterID+"/"+self.incrReqID()+"/"+self.accessToken

    # 로그인
    def login(self, request):
        formData = '{"param":{"accessToken":"'+self.accessToken+'","accessUser":"'+self.pid+'","accessType":"dummy","udid":"3dac4083f473d1604e5f017ae4512ad77309c795","device":"COM-PC","locale":"Korean","model":"Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz (16334 MB)","osVersion":"Windows 7 Service Pack 1 (6.1.7601) 64bit","language":"english","platform":"WindowsEditor","appVersion":"0.0.1","isNotification":"1"}}'
        return request.POST("/auth/login/"+self.incrReqID(), formData)

    # 로비 진입
    def entrance(self, request):
        formData = ''
        return request.POST(self.getUri("/lobby/entrance"), formData)

    # 데일리 스핀
    def dailyspinretry(self, request):
        formData = ''
        return request.POST(self.getUri("/lobby/dailyspinretry"), formData)

    # 리더보드(랭킹)
    def leaderboards(self, request):
        formData = ''
        return request.POST(self.getUri("/lobby/leaderboards"), formData)

    # 프로필 조회
    def profile(self, request):
        formData = '{"param":{"characterID":"'+self.characterID+'"}}'
        return request.POST(self.getUri("/user/profile"), formData)

    # 프로필 수정 (나이)
    def setprofile(self, request):
        formData = '{"param":{"characterID":"'+self.characterID+'","characterAge":"'+str(random.randrange(1,99))+'"}}'
        return request.POST(self.getUri("/user/setprofile"), formData)

    # 메시지함 조회
    def getmessage(self, request):
        formData = '{"param":{"postBox":[]}}'
        return request.POST(self.getUri("/user/getmessage"), formData)

    # 슬롯 입장
    def join(self, request, slotId):
        formData = '{"param":{"slotId":'+str(slotId)+'}}'
        return request.POST(self.getUri("/slot/join"), formData)

    # 슬롯 릴 플레이
    def addreel(self, request, slotId, tableId):
        formData = '{"param":{"slotId":'+str(slotId)+',"tableId":"'+tableId+'","bet":100,"line":9,"freespin":0}}'
        return request.POST(self.getUri("/slot/addreel"), formData)

    # 슬롯 현재 상태 조회
    def tableinfo(self, request, slotId, tableId):
        formData = '{"param":{"slotId":'+str(slotId)+',"tableId":"'+tableId+'"}}'
        return request.POST(self.getUri("/slot/tableinfo"), formData)

    # 슬롯 나가기
    def leave(self, request, slotId, tableId):
        formData = '{"param":{"slotId":'+str(slotId)+',"tableId":"'+tableId+'"}}'
        return request.POST(self.getUri("/slot/leave"), formData)

    # 로비 대기 (룸 초대목록 조회)
    def recentdata(self, request):
        formData = ''
        return request.POST(self.getUri("/lobby/recentdata"), formData)

    # 친구목록 조회
    def getfriends(self, request):
        formData = ''
        return request.POST(self.getUri("/friend/getfriends"), formData)

    # 친구 요청 목록 조회
    def getinvitelist(self, request):
        formData = ''
        return request.POST(self.getUri("/friend/getinvitelist"), formData)

    # 테스트용 유저 칩, 티켓 설정(1000000000)
    def setUserChipAndTicket(self, request):
        formData = ''
        return request.POST(self.getUri("/ngrinder/setuserchipandticket"), formData)

    # 친구추가
    def addfriend(self, request):
        formData = '{"param":{"fpid":"'+str(self.fpid)+'"}}'
        return request.POST(self.getUri("/ngrinder/addfriend"), formData)

    # 친구삭제
    def removefriend(self, request):
        formData = '{"param":{"fpid":"'+str(self.fpid)+'"}}'
        return request.POST(self.getUri("/ngrinder/removefriend"), formData)

    # 인앱결제 스토어 조회
    def getstore(self, request):
        formData = ''
        return request.POST(self.getUri("/store/getstore"), formData)

    # 룸에서 기프트상품 구매
    def buygiftshop(self, request, tableId):
        formData = '{"param":{"id":"1","tableId":"'+str(tableId)+'","characterIds":["'+self.characterID+'"]}}'
        return request.POST(self.getUri("/store/buygiftshop"), formData)

    # 구글 결제 준비
    def googlepurchaseprepare(self, request):
        formData = '{"param":{"productId":"1"}}'
        return request.POST(self.getUri("/store/googlepurchaseprepare"), formData)

    # 구글 결제
    def googlepurchase(self, request, orderId, transactionId, purchaseTime, ConsumingResult):
        productId = 'com.nzincorp.jackpotrush.1'
        purchaseToken = 'purchaseToken'
        email = 'google@redstargames.co.kr'
        formData = '{"param":{"orderId":"'+orderId+'","purchaseState":"0","productId":"'+productId+'","purchaseTime":"purchaseTime","purchaseToken":"purchaseToken","ConsumingResult":"'+ConsumingResult+'","email":"email@redstargames.co.kr","transactionId":"'+transactionId+'","developerPayload":""}}'
        return request.POST(self.getUri("/store/googlepurchase"), formData)